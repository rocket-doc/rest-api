Мультиязычность
===

Язык отправляется в виде заголовка `Accept-Language`.

Например:

```json
{
		"Accept-Language": "en"
}
```

Это значит, что ответ будет приходить в выбранном переводе.
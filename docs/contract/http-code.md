HTTP-код
===

## Поведение

Статус коды:

* `2xx` - запрос успешно обработан
* `4xx` - ошибка, связанная с переданными параметрами
* `5xx` - ошибка сервера, на которую не может повлиять клиент

## Формат ошибок

В случае, если ошибка имеет статус-код не 422, то в теле возрващается пояснение и внутренний код ошибки (опционально):

```json
{
    "message": "Bad query parameters",
    "code": 0
}
```

## Статус-коды

* [200 OK](http-code/200.md)
* [201 Created](http-code/201.md)
* [204 No Content](http-code/204.md)
* [400 Bad Request](http-code/400.md)
* [401 Unauthorized](http-code/401.md)
* [403 Forbidden](http-code/403.md)
* [404 Not Found](http-code/404.md)
* [422 Unprocessable Entity](http-code/422.md)
* [429 Too Many Requests](http-code/429.md)
* [500 Internal Server Error](http-code/500.md)
